# Dualgraph

A library for building a dualgraph from triangles.

The graph structure allows you to traverse the triangles efficiently long their edges and neighbors.  
For an in-depth explanation of the dualgraph concept, visit the great [blogpost](https://www.redblobgames.com/x/1722-b-rep-triangle-meshes/) from redblobgames

## Features

- triangle and voronoi traversal functions
- based upon very memory efficient storage
- ghost node wrapping the graph behind the back (b-rep)
- built with [rtriangulate](https://crates.io/crates/rtriangulate) library (likely to change in the future)

## Usage

Have a look at the module documentation [TODO link]().

A quick overview:

- Use the builder module to build the graph from points, triangles or more
- The Geometry can be arbitary types of your choosing
- The graph itself only uses idices and offers a lot of functions for traversal

## Upcoming

- generate from triangle-list
- export voronoi polygons
- lloyd relaxation - export voronoi and rerun delaunay
- implement a delaunay algorithm
- make dynamic delaunay possible (for damage detection)
