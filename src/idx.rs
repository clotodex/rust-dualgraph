//! Newtype style indices without overhead
//! Allows:
//!
//! - type validation
//! - convenience functions
//! - From,Into usize conversions
//! - Copy semantics
//!
//! These indices are used in the IndexGraph

/// The index of an edge together with convenience functions  
/// Every 3 edges build a triangle
///
/// # Example
///
/// ```
/// # use dualgraph::idx::{EdgeIdx, TriangleIdx};
/// // From,Into conversions
/// let e: EdgeIdx = 1.into();
/// // Equality and ordering
/// assert!(e > EdgeIdx(0));
///
/// assert_eq!(EdgeIdx(0), EdgeIdx(1).prev());
/// assert_eq!(EdgeIdx(2), EdgeIdx(1).next());
///
/// assert_eq!(TriangleIdx(0), EdgeIdx(1).triangle());
/// assert_eq!(TriangleIdx(1), EdgeIdx(3).triangle());
///
/// ```
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct EdgeIdx(pub usize); //or Side

/// Index of a point
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PointIdx(pub usize); //or Seed or Region

/// Index of a triangle
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct TriangleIdx(pub usize); //or Center

impl EdgeIdx {
    /// Returns the index of the previous edge (belonging to the same triangle)  
    /// Wraps around the triangle
    ///
    /// # Example
    ///
    /// ```
    /// # use dualgraph::idx::{EdgeIdx, TriangleIdx};
    /// assert_eq!(EdgeIdx(0), EdgeIdx(1).prev());
    /// assert_eq!(EdgeIdx(1), EdgeIdx(2).prev());
    /// assert_eq!(EdgeIdx(2), EdgeIdx(0).prev());
    ///
    /// assert_eq!(EdgeIdx(5), EdgeIdx(3).prev());
    /// ```
    #[inline]
    pub fn prev(self) -> Self {
        if self.0 % 3 == 0 {
            EdgeIdx(self.0 + 2)
        } else {
            EdgeIdx(self.0 - 1)
        } //TODO can be replaced with binary & operator
    }
    /// Returns the index of the next edge (belonging to the same triangle)  
    /// Wraps around the triangle
    ///
    /// # Example
    ///
    /// ```
    /// # use dualgraph::idx::{EdgeIdx, TriangleIdx};
    /// assert_eq!(EdgeIdx(0), EdgeIdx(1).prev());
    /// assert_eq!(EdgeIdx(1), EdgeIdx(2).prev());
    /// assert_eq!(EdgeIdx(2), EdgeIdx(0).prev());
    ///
    /// assert_eq!(EdgeIdx(5), EdgeIdx(3).prev());
    /// ```
    #[inline]
    pub fn next(self) -> Self {
        if self.0 % 3 == 2 {
            EdgeIdx(self.0 - 2)
        } else {
            EdgeIdx(self.0 + 1)
        } //TODO can be replaced with binary & operator
    }

    /// Gives an index for the corresponding triangle of this index  
    ///
    /// # Example
    ///
    /// ```
    /// # use dualgraph::idx::{EdgeIdx, TriangleIdx};
    ///
    /// assert_eq!(TriangleIdx(0), EdgeIdx(0).triangle());
    /// assert_eq!(TriangleIdx(0), EdgeIdx(1).triangle());
    /// assert_eq!(TriangleIdx(0), EdgeIdx(2).triangle());
    ///
    /// assert_eq!(TriangleIdx(1), EdgeIdx(3).triangle());
    /// assert_eq!(TriangleIdx(1), EdgeIdx(4).triangle());
    /// assert_eq!(TriangleIdx(1), EdgeIdx(5).triangle());
    /// ```
    #[inline]
    pub fn triangle(self) -> TriangleIdx {
        TriangleIdx(self.0 / 3)
    }
}

impl From<usize> for EdgeIdx {
    fn from(u: usize) -> EdgeIdx {
        EdgeIdx(u)
    }
}
impl From<usize> for PointIdx {
    fn from(u: usize) -> PointIdx {
        PointIdx(u)
    }
}
impl From<usize> for TriangleIdx {
    fn from(u: usize) -> TriangleIdx {
        TriangleIdx(u)
    }
}
impl Into<usize> for EdgeIdx {
    fn into(self) -> usize {
        self.0
    }
}
impl Into<usize> for PointIdx {
    fn into(self) -> usize {
        self.0
    }
}
impl Into<usize> for TriangleIdx {
    fn into(self) -> usize {
        self.0
    }
}
