#![warn(clippy)]
#![allow(dead_code)]
#![allow(unused_variables)]
#![warn(missing_docs)]
//#[deny(missing_docs_in_private_items)]
//#![deny(missing_docs,
//        missing_debug_implementations, missing_copy_implementations,
//        trivial_casts, trivial_numeric_casts,
//        unsafe_code,
//        unstable_features,
//        unused_import_braces, unused_qualifications)]

//! Create dualgraphs for delaunay triangulations with b-rep features
//!
//! [Gitlab Link](https://gitlab.com/clotodex/rust-dualgraph)
//!
//! TODO describe modules and functions and general usage etc
//! TODO fix links to use reference style

#[macro_use]
extern crate shrinkwraprs;

pub mod builder;
pub mod graph;
pub mod idx;

use graph::IndexGraph;
use idx::*;

use std::ops::Index;

//TODO deny missing doc
//TODO split into more functions and modules
//TODO parallelize
//TODO macro for index
//TODO extend typed_index_derive to handle Option<...> and indexing multiple collections

//TODO think about naming convention

/// The main struct holding the Geometry as well as the IndexGraph
///
/// This struct uses the underlying graph structure to index directly into the geometry and thus
/// makes the traversal of the triangles simpler  
/// Dereferencing the DualGraph gives you direct access to all IndexGraph methods
///
/// Have a look at the builder module to build the index structure from an existing Geometry
#[derive(Shrinkwrap)]
#[shrinkwrap(mutable)]
pub struct DualGraph<P, C> {
    /// The geometry holding the actual points and centers
    pub geometry: Geometry<P, C>,

    /// Graph structure managing indices and methods to traverse the triangle geometry
    #[shrinkwrap(main_field)]
    pub dualgraph: IndexGraph,
}

/// A container holding the points for the triangles and the center of each triangle
///
/// It can be created manually or with the builder module  
/// The struct is fully generic, allowing you to store more than 2D points
///
/// The struct can be indexed directly with PointIdx and TriangleIdx giving points or centers
/// respectively
#[derive(Debug)]
pub struct Geometry<P, C> {
    /// The triangle corners
    pub points: Vec<P>,

    /// The triangle centers
    pub centers: Vec<C>,
}

// awesome method overloading with newtypes and traits :D
impl<P, C> Index<PointIdx> for Geometry<P, C> {
    type Output = P;
    fn index(&self, p: PointIdx) -> &P {
        &self.points[p.0]
    }
}
impl<P, C> Index<TriangleIdx> for Geometry<P, C> {
    type Output = C;
    fn index(&self, t: TriangleIdx) -> &C {
        &self.centers[t.0]
    }
}

impl Index<EdgeIdx> for Vec<Option<EdgeIdx>> {
    type Output = Option<EdgeIdx>;
    #[inline]
    fn index(&self, edge: EdgeIdx) -> &Option<EdgeIdx> {
        &self[edge.0]
    }
}
use std::ops::IndexMut;
impl IndexMut<EdgeIdx> for Vec<Option<EdgeIdx>> {
    fn index_mut(&mut self, index: EdgeIdx) -> &mut Option<EdgeIdx> {
        &mut self[index.0]
    }
}

//TODO test invariants, out of bounds, ghost etc
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert!(true);
    }
}
