//! A module for building the DualGraph
//!
//! GeometryBuilder -> DualGraphBuilder
//!
//! TODO examples

use super::DualGraph;
use super::Geometry;
use super::IndexGraph;
use idx::*;

/// Access to x and y of a point-like structure
pub trait Point2D {
    /// The x coordinate
    fn x(&self) -> f64;
    /// The y coordinate
    fn y(&self) -> f64;
}

/// Different types of centers for triangles  
/// TODO add link to center explanation site
pub enum CenterFunction {
    /// Centroid as defined [here][1]
    ///
    /// [1]: https://www.mathopenref.com/trianglecentroid.html
    Centroid,
    /// Circumcenter as defined [here][1]  
    /// **unimplemented**
    ///
    /// [1]: https://www.mathopenref.com/trianglecircumcenter.html
    Circumcenter,
    /// Incenter as defined [here][1]  
    /// **unimplemented**
    ///
    /// [1]: https://www.mathopenref.com/triangleincenter.html
    Incenter,
    /// Orthocenter as defined [here][1]  
    /// **unimplemented**
    ///
    /// [1]: https://www.mathopenref.com/triangleorthocenter.html
    Orthocenter,
    //Other(FnOnce<...>)
}

impl CenterFunction {
    /// computes the center based on the variant  
    /// takes a triangle - 3 Point2Ds - as input  
    /// returns a tuple representing the center
    pub fn center<P>(&self, t1: &P, t2: &P, t3: &P) -> (f64, f64)
    where
        P: Point2D,
    {
        match self {
            CenterFunction::Centroid => (
                (t1.x() + t2.x() + t3.x()) / 3.,
                (t1.y() + t2.y() + t3.y()) / 3.,
            ),
            _ => unimplemented!(),
        }
    }
}

//TODO use dyn Traits - small overhead but more memory efficient when using different types
/// Builder pattern preparing the geometry for DualGraphBuilder
pub struct GeometryBuilder<P: Point2D> {
    center_function: CenterFunction, //TODO change to an FnOnce - can still use CenterFunction::..::center()
    points: Vec<P>,
    triangles: Vec<(usize, usize, usize)>,
}

impl<P: Point2D> GeometryBuilder<P> {
    /// Create a new builder with a function to find the center of a triangle
    ///
    /// This will initialize all fields to empty vectors
    pub fn new(center_function: CenterFunction) -> Self {
        Self {
            center_function,
            points: Vec::new(),
            triangles: Vec::new(),
        }
    }

    /// Add more triangles to the geometry
    ///
    /// Triangles are defined by indices pointing into the points array
    pub fn add_triangles(
        mut self,
        points: impl IntoIterator<Item = P>,
        triangles: impl IntoIterator<Item = (usize, usize, usize)>,
    ) -> Self {
        self.points.extend(points);
        self.triangles.extend(triangles);
        self
    }

    /// Finds the center for all registered triangles using the center_function
    fn centers<C, F>(&self, mapping_function: F) -> Vec<C>
    where
        F: FnMut((f64, f64)) -> C,
    {
        self.triangles
            .iter()
            .map(|(t1, t2, t3)| self.center_function.center(&self.points[*t1], &self.points[*t2], &self.points[*t3]))
            //return iterator instead
            .map(mapping_function)
            .collect()
    }

    /// Builds the geometry and constructs a DualGraphBuilder
    ///
    /// TODO This will likely change in the future, since this gives no access to the built
    /// Geometry
    pub fn dual_graph<C, F>(self, mapping_function: F) -> DualGraphBuilder<P, C>
    where
        F: FnMut((f64, f64)) -> C,
    {
        //TODO have seperate builder function for exporting the geometry
        let centers = self.centers(mapping_function);
        let geometry = Geometry {
            points: self.points,
            centers,
        };
        DualGraphBuilder::new(geometry, self.triangles)
    }
}

/// Builder pattern for generating the IndexGraph from the geometry  
/// It builds a DualGraph holding both
pub struct DualGraphBuilder<P, C> {
    geometry: Geometry<P, C>,
    triangles: Vec<(usize, usize, usize)>,
}

impl<P, C> DualGraphBuilder<P, C> {
    /// Constructs a new builder with a geometry and indices for constructing triangles from points
    pub fn new(geometry: Geometry<P, C>, triangles: Vec<(usize, usize, usize)>) -> Self {
        Self {
            geometry,
            triangles,
        }
    }

    //TODO split into IndexGraphBuilder to allow building ghost structure without having geometry
    /// Build up the IndexGraph (including the Ghost structure) and package everything in a
    /// DualGraph
    pub fn build(self) -> DualGraph<P, C> {
        let mut full_edges: Vec<(usize, usize)> = vec![];

        let mut edges: Vec<PointIdx> = vec![];
        let mut opposites: Vec<Option<EdgeIdx>> = vec![];
        let mut starts: Vec<Option<usize>> = vec![None; self.geometry.points.len()];

        //TODO use methods of index types instead of raw usize
        for (t1, t2, t3) in &self.triangles {
            let e_ind = edges.len();
            edges.push((*t1).into());
            edges.push((*t2).into());
            edges.push((*t3).into());

            full_edges.push((*t1, *t2));
            full_edges.push((*t2, *t3));
            full_edges.push((*t3, *t1));

            //TODO replace with fancy optional map
            if let Some(index) = full_edges
                .iter()
                .position(|(from, to)| from == t2 && to == t1)
            {
                opposites.push(Some(index.into())); //actually index e_ind
                opposites[index] = Some(e_ind.into());
            } else {
                opposites.push(None);
            }
            if let Some(index) = full_edges
                .iter()
                .position(|(from, to)| from == t3 && to == t2)
            {
                opposites.push(Some(index.into())); //actually index e_ind + 1
                opposites[index] = Some((e_ind + 1).into());
            } else {
                opposites.push(None);
            }
            if let Some(index) = full_edges
                .iter()
                .position(|(from, to)| from == t1 && to == t3)
            {
                opposites.push(Some(index.into())); //actually index e_ind + 2
                opposites[index] = Some((e_ind + 2).into());
            } else {
                opposites.push(None);
            }

            //checks if leftmost edge is already contained
            //TODO prevent overriding of index 0
            if starts[*t1].is_none() {
                starts[*t1] = Some(e_ind);
            }
            if starts[*t2].is_none() {
                starts[*t2] = Some(e_ind + 1);
            }
            if starts[*t3].is_none() {
                starts[*t3] = Some(e_ind + 2);
            }
        }
        let starts: Vec<EdgeIdx> = starts
            .into_iter()
            .flat_map(|e| e)
            .map(|e| e.into())
            .collect();

        debug_assert_eq!(
            edges.len(),
            opposites.len(),
            "edges & opposites differ in length"
        );

        println!("edges: {:?}", edges);
        println!("opposites: {:?}", opposites);

        let opposites = {
            // for each missing opposite:
            //      add edges entry pointing to actual point
            //      add 2 edge entries pointing to the seed point
            let num_real_edges = edges.len();
            let ghost_point = PointIdx(starts.len());
            let (num_missing_opposites, any_edge_missing_an_opposite) = {
                let missing_opposites: Vec<EdgeIdx> = opposites
                    .iter()
                    .enumerate()
                    .filter(|(_, o)| o.is_none())
                    .map(|(i, _)| EdgeIdx(i))
                    .collect();
                println!("missing: ");
                for i in &missing_opposites {
                    //.map(|i| edges[i.0].0) {
                    print!("{:?}, ", i)
                }
                (missing_opposites.len(), missing_opposites[0])
            };

            debug_assert!(num_missing_opposites >= 3); //otherwise a new point does not make sense

            //TODO add to starts with last or first ghost edge

            //notify vectors of increased capacity (arg=additional)
            edges.reserve_exact(3 * num_missing_opposites);
            opposites.reserve_exact(3 * num_missing_opposites);

            let last_triangle = EdgeIdx(num_real_edges + 3 * num_missing_opposites - 1);
            let first_triangle = EdgeIdx(num_real_edges + 1);

            let mut edge: Option<EdgeIdx> = None;
            loop {
                //first / last iteration gate
                if edge.is_none() {
                    //first iteration
                    edge = Some(any_edge_missing_an_opposite);
                    debug_assert_eq!(
                        None,
                        opposites[edge.unwrap()],
                        "initial edge had an opposite..."
                    );
                } else if edge.unwrap() == any_edge_missing_an_opposite {
                    break;
                }
                debug_assert_eq!(
                    None,
                    opposites[edge.unwrap()],
                    "next edge had an opposite.."
                );

                let e = edge.unwrap(); //TODO create as return value from if statement?
                println!("current edge: {:?}", e);

                //create ghost triangle

                //create ghost opposite
                let p = edges[e.next()];
                let new = EdgeIdx(edges.len());
                edges.push(p);
                opposites.push(Some(e));
                opposites[e] = Some(new); // == opposites.len()

                //TODO move first and last check outside of for loop??

                //create ghost edge #2
                let p = edges[e];
                let new = EdgeIdx(edges.len());
                edges.push(p);
                //connect to edge of the previous ghost triangle
                //if first triangle - wrap to the last (still to be created) triangle
                opposites.push(Some(if new == first_triangle {
                    last_triangle
                } else {
                    EdgeIdx(new.0 - 2)
                }));

                //create ghost edge #3
                let new = EdgeIdx(edges.len());
                edges.push(ghost_point);
                //connect to edge of the next ghost triangle
                //if last triangle - wrap to the last (still to be created) triangle
                opposites.push(Some(if new == last_triangle {
                    first_triangle
                } else {
                    EdgeIdx(new.0 + 2)
                }));

                //traverse to next edge without a missing opposite
                //TODO check if new edge actually has an opposite missing
                edge = Some(e.next());
                loop {
                    if
                    /*edge.unwrap() == e.next() ||*/
                    edge.unwrap() == any_edge_missing_an_opposite {
                        println!("assign break");
                        edge = Some(any_edge_missing_an_opposite);
                        break;
                    }
                    if let Some(o) = opposites[edge.unwrap()] {
                        edge = Some(o.next());
                    } else {
                        println!("no opposite break");
                        break;
                    }
                }
            }

            if opposites.iter().any(|o| o.is_none()) {
                panic!("ghost creation failed");
            } else {
                println!("all fine!");
            }
            opposites.into_iter().flat_map(|o| o).collect()
        };

        DualGraph {
            geometry: self.geometry,
            dualgraph: IndexGraph {
                edges,
                opposites,
                starts,
            },
        }
    }
}
