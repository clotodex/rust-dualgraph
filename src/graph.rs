//! A graph structure for indexing triangles and centers
//! The graph allows traversal:
//!
//! - to neighbors
//! - along edges

use idx::{EdgeIdx, PointIdx, TriangleIdx};
use std::ops::Index;

type Edges = Vec<PointIdx>;
type Opposites = Vec<EdgeIdx>;
type Starts = Vec<EdgeIdx>;

//TODO use derive crate for this (index derive or similar)
//Edge
impl Index<EdgeIdx> for Edges {
    type Output = PointIdx;
    #[inline]
    fn index(&self, edge: EdgeIdx) -> &PointIdx {
        &self[edge.0]
    }
}
impl Index<EdgeIdx> for Opposites {
    type Output = EdgeIdx;
    #[inline]
    fn index(&self, edge: EdgeIdx) -> &EdgeIdx {
        &self[edge.0]
    }
}

//Point
impl Index<PointIdx> for Starts {
    type Output = EdgeIdx;
    #[inline]
    fn index(&self, point: PointIdx) -> &EdgeIdx {
        &self[point.0]
    }
}

/// A dual graph of half edges, managing triangle indices
#[derive(Debug)]
pub struct IndexGraph {
    /// All edges of the graph, maps the edge index to its origin point  
    /// edges[edge_ind] -> point_ind  
    /// Edges are stored in triangles, like GL_TRIANGLE_STRIP  
    /// edge 0 = line from geometry.points[edges[0]] to geometry.points[edges[1]]  
    /// edge 1 = line from geometry.points[edges[1]] to geometry.points[edges[2]]  
    /// edge 2 = line from geometry.points[edges[2]] to geometry.points[edges[0]]  
    /// Hence triangles can be deduced from this list
    pub edges: Edges, //edge index origins (edge->origin_point)
    /// a simple way to find triangles that are next to each other
    pub opposites: Opposites, //edge->edge (find the opposite direction half-edge)
    /// one starting edge for each point
    pub starts: Starts, //point -> edge (leftmost edge (for exterior points))
}

//TODO fix to include ghost tests
impl IndexGraph {
    //TODO add fn that checks if current edge is a ghost
    fn ghost_index(&self) -> EdgeIdx {
        self.edges.len().into()
    }

    ///Returns all edges of node 's'
    pub fn edge_loop(&self, s: PointIdx) -> Vec<EdgeIdx> {
        let mut result = vec![];

        let e0 = self.starts[s];
        let mut e = e0;
        loop {
            result.push(e);
            let e1 = self.opposites[e];
            if e1 == self.ghost_index() {
                break;
            } else {
                e = e1.next();
            }
            if e == e0 {
                break;
            }
        }
        result
    }

    ///Gives the node index the edge is pointing to
    pub fn edge_end(&self, e_ind: EdgeIdx) -> PointIdx {
        if e_ind >= self.ghost_index() {
            panic!("you just saw a ghost!");
        }
        self.edges[e_ind.next()]
    }

    pub fn borders() -> Vec<usize> {
        unimplemented!()
    }
}
