all: doc

test:
	cargo test

doc: test
	cargo doc --no-deps

dev-doc: test
	cargo rustdoc --open -- --document-private-items

check:
	cargo +nightly clippy

clean:
	cargo clean
