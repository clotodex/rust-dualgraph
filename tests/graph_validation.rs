extern crate dualgraph;
extern crate rtriangulate;

use dualgraph::builder::*;
use dualgraph::*;
use rtriangulate::*;

#[derive(Debug)]
struct MyPoint {
    x: f64,
    y: f64,
}
impl MyPoint {
    pub fn new(x: f64, y: f64) -> Self {
        Self { x, y }
    }
}
impl Point<f64> for MyPoint {
    fn x(&self) -> f64 {
        self.x
    }
    fn y(&self) -> f64 {
        self.y
    }
}
impl Point2D for MyPoint {
    fn x(&self) -> f64 {
        self.x
    }
    fn y(&self) -> f64 {
        self.y
    }
}

#[test]
fn validate() {
    const WIDTH: usize = 20;
    const HEIGHT: usize = 20;

    let mut points = vec![];
    points.push(MyPoint::new(
        100. / 2. / 100. * (WIDTH as f64),
        100. / 2. / 100. * (HEIGHT as f64),
    ));
    points.push(MyPoint::new(
        70. / 2. / 100. * (WIDTH as f64),
        140. / 2. / 100. * (HEIGHT as f64),
    ));
    points.push(MyPoint::new(
        130. / 2. / 100. * (WIDTH as f64),
        140. / 2. / 100. * (HEIGHT as f64),
    ));
    points.push(MyPoint::new(
        160. / 2. / 100. * (WIDTH as f64),
        70. / 2. / 100. * (HEIGHT as f64),
    ));
    points.push(MyPoint::new(
        30. / 2. / 100. * (WIDTH as f64),
        80. / 2. / 100. * (HEIGHT as f64),
    ));
    points.push(MyPoint::new(
        90. / 2. / 100. * (WIDTH as f64),
        60. / 2. / 100. * (HEIGHT as f64),
    ));
    points.push(MyPoint::new(
        20. / 2. / 100. * (WIDTH as f64),
        120. / 2. / 100. * (HEIGHT as f64),
    ));
    points.push(MyPoint::new(
        170. / 2. / 100. * (WIDTH as f64),
        130. / 2. / 100. * (HEIGHT as f64),
    ));

    // If needed, you can sort your points like this:
    //TODO check if this changes anything
    points.sort_unstable_by(rtriangulate::sort_points);

    for (i, p) in points.iter().enumerate() {
        println!("s{} = {:?}", i, p);
    }

    let triangles: Vec<(usize, usize, usize)> = triangulate(&points)
        .unwrap()
        .into_iter()
        .map(|Triangle(t1, t2, t3)| (t1, t2, t3))
        .collect();

    println!("triangles:");
    for triangle in &triangles {
        println!("\t{:?}", triangle);
    }

    let dg = GeometryBuilder::new(CenterFunction::Centroid)
        .add_triangles(points, triangles)
        .dual_graph(|(x, y)| MyPoint::new(x, y))
        .build();
}
